## Albanero-test-backend

> albanero test code backend

## Build Setup

```bash
# install dependencies
$ npm install

# database
In case of Linux System, install mongodb and start
$ sudo service mongod start
Open another terminal and run
$ mongo

# launch server
$ npm start
```
