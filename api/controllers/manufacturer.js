const mongoose = require("mongoose");

const SupplyRequest = require("../models/supply-request");

exports.manufactuer_supply_request_create = (req, res, next) => {
  const supplyRequest = new SupplyRequest({
    _id: Math.random()
      .toString(36)
      .substr(2, 9),
    supplierName: req.body.supplierName,
    supplierEmail: req.body.supplierEmail,
    supplierContact: req.body.supplierContact,
    materialName: req.body.materialName,
    materialPartNumber: req.body.materialPartNumber,
    isAcceptedBySupplier: req.body.isAcceptedBySupplier,
    createdBy: req.body.createdBy,
    manufacturerAddress: req.body.manufacturerAddress,
    status: req.body.status,
    attachement: req.file
  });
  supplyRequest
    .save()
    .then(result => {
      console.log(result);
      res.status(201).json({
        message: "Request created successfully",
        createdRequest: result
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({ error: err });
    });
};

exports.manufacturer_get_supply_pending_requests = (req, res, next) => {
  SupplyRequest.find({
    createdBy: req.body.createdBy,
    isAcceptedBySupplier: false
  })
    .select("-__v")
    .exec()
    .then(result => {
      const response = { requests: result, count: result.length };
      console.log(result);
      res.status(200).json(response);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({ error: err });
    });
};

exports.manufacturer_get_supply_accepted_requests = (req, res, next) => {
  SupplyRequest.find({
    createdBy: req.body.createdBy,
    isAcceptedBySupplier: true
  })
    .select("-__v")
    .exec()
    .then(result => {
      const response = { requests: result, count: result.length };
      console.log(result);
      res.status(200).json(response);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({ error: err });
    });
};

exports.manufacturer_update_request_status = (req, res, next) => {
  const id = req.params.requestId;
  const props = req.body;
  SupplyRequest.updateOne({ _id: id }, props)
    .exec()
    .then(result => {
      res.status(200).json({ message: "Status updated" });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({ error: err });
    });
};

exports.manufactuer_get_request = (req, res, next) => {
  const id = req.params.requestId;
  SupplyRequest.findById(id)
    .select("-__v")
    .exec()
    .then(result => {
      res.status(200).json({ message: "get request success", data: result });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({ error: err });
    });
};

exports.manufactuer_get_request_addresses = (req, res, next) => {
  const id = req.params.requestId;
  SupplyRequest.findById(id)
    .select("BOM supplierAddress manufacturerAddress")
    .exec()
    .then(result => {
      res.status(200).json({ message: "get request success", data: result });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({ error: err });
    });
};
