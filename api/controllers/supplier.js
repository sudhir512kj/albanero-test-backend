const mongoose = require("mongoose");

const SupplyRequest = require("../models/supply-request");

exports.supplier_get_supply_pending_requests = (req, res, next) => {
  console.log(req.body);
  SupplyRequest.find({
    supplierEmail: req.email,
    isAcceptedBySupplier: false
  })
    .select("-__v")
    .exec()
    .then(result => {
      const response = { requests: result, count: result.length };
      console.log(result);
      res.status(200).json(response);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({ error: err });
    });
};

exports.supplier_get_supply_accepted_requests = (req, res, next) => {
  SupplyRequest.find({
    supplierEmail: req.email,
    isAcceptedBySupplier: true
  })
    .select("-__v")
    .exec()
    .then(result => {
      const response = { requests: result, count: result.length };
      console.log(result);
      res.status(200).json(response);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({ error: err });
    });
};

exports.supplier_accept_request = (req, res, next) => {
  const id = req.params.requestId;
  const props = req.body;
  SupplyRequest.updateOne({ _id: id }, props)
    .exec()
    .then(result => {
      res.status(200).json({ message: "Request accepted" });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({ error: err });
    });
};

exports.supplier_add_bom = (req, res, next) => {
  const id = req.params.requestId;
  const props = req.body;
  SupplyRequest.updateOne({ _id: id }, { $push: { BOM: props } })
    .exec()
    .then(result => {
      res.status(200).json({ message: "update successfull", data: result });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({ error: err });
    });
};
