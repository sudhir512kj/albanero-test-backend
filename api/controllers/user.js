const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const googleMapsClient = require("@google/maps").createClient({
  key: "AIzaSyAYOl9hi9B4mfq2wLkabIcrrL8_458lC78",
  Promise: Promise
});

const User = require("../models/user");

exports.user_signup = (req, res, next) => {
  User.find({ email: req.body.email })
    .exec()
    .then(user => {
      if (user.length >= 1) {
        res.status(409).json({
          message: "User with this email already exists!!"
        });
      } else {
        bcrypt.hash(req.body.password, 10, (err, hash) => {
          if (err) {
            return res.status(500).json({
              error: err
            });
          } else {
            const user = new User({
              _id: new mongoose.Types.ObjectId(),
              firstName: req.body.firstName,
              lastName: req.body.lastName,
              email: req.body.email,
              password: hash,
              country: req.body.country,
              state: req.body.state,
              companyName: req.body.companyName,
              companyContact: req.body.companyContact,
              buyer: req.body.buyer,
              categoryIDs: req.body.categoryIDs
            });
            user
              .save()
              .then(result => {
                console.log(result);
                res.status(201).json({
                  message: "User created!!"
                });
              })
              .catch(err => {
                console.log(err);
                res.status(500).json({
                  error: err
                });
              });
          }
        });
      }
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({ error: err });
    });
};

exports.user_login = (req, res, next) => {
  User.find({ email: req.body.email })
    .exec()
    .then(user => {
      if (user.length < 1) {
        return res.status(401).json({
          message: "Auth failed!!"
        });
      }
      bcrypt.compare(req.body.password, user[0].password, (err, result) => {
        if (err) {
          return res.status(401).json({
            message: "Wrong Password!!"
          });
        }
        if (result) {
          const token = jwt.sign(
            {
              email: user[0].email,
              userId: user[0]._id
            },
            process.env.JWT_KEY,
            {
              expiresIn: 86400
            }
          );
          return res.status(200).json({
            message: "Auth successfull",
            data: {
              token: token,
              email: user[0].email
            }
          });
        }
        res.status(401).json({
          message: "Wrong Password!!"
        });
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

exports.user_get_account_details = (req, res, next) => {
  User.findOne({ email: req.email })
    .select(
      "firstName lastName email country state companyName companyContact buyer"
    )
    .exec()
    .then(user => {
      return res.status(200).json({
        message: "Profile get successfully",
        data: user
      });
    })
    .catch(() => {
      res.status(500).json({ message: "No User found!!" });
    });
};

exports.user_get_location = (req, res, next) => {
  googleMapsClient
    .geocode({ address: req.body.address })
    .asPromise()
    .then(response => {
      res.status(200).json(response.json.results);
    })
    .catch(() => {
      res.status(500).json({ message: "Failed to get location details!!" });
    });
};
