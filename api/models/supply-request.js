const mongoose = require("mongoose");

const supplyRequestSchema = mongoose.Schema({
  _id: {
    type: String
  },
  supplierName: {
    type: String,
    required: [true, "Supplier Name required"]
  },
  supplierEmail: {
    type: String,
    required: [true, "Supplier Email required"],
    match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
  },
  supplierContact: {
    type: String,
    validate: {
      validator: function(v) {
        return /\d{3}-\d{3}-\d{4}/.test(v);
      },
      message: props => `${props.value} is not a valid phone number!`
    },
    required: [true, "Supplier Contact Num required"]
  },
  materialName: {
    type: String,
    required: [true, "Material Name required"]
  },
  materialPartNumber: {
    type: Number,
    required: [true, "Material Part Number required"],
    min: 1
  },
  isAcceptedBySupplier: {
    type: Boolean,
    default: false
  },
  createdBy: {
    type: String
  },
  manufacturerAddress: {
    type: String
  },
  supplierAddress: {
    type: String
  },
  createdAt: {
    type: Date,
    value: new Date()
  },
  status: {
    type: String
  },
  attachement: {
    type: String
  },
  BOM: {
    type: Array
  }
});

module.exports = mongoose.model("SupplyRequest", supplyRequestSchema);
