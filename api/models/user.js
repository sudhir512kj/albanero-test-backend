const mongoose = require("mongoose");

const userSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  firstName: {
    type: String,
    required: [true, "First Name required"]
  },
  lastName: {
    type: String
  },
  email: {
    type: String,
    required: [true, "Email required"],
    unique: true,
    match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
  },
  password: {
    type: String,
    required: [true, "Password required"]
  },
  country: {
    type: String,
    enum: ["india"],
    required: [true, "Country required"]
  },
  state: {
    type: String,
    enum: ["rajasthan", "gujarat", "haryana", "delhi"],
    required: [true, "State required"]
  },
  companyName: {
    type: String,
    required: [true, "Company Name required"]
  },
  companyContact: {
    type: String,
    validate: {
      validator: function(v) {
        return /\d{3}-\d{3}-\d{4}/.test(v);
      },
      message: props => `${props.value} is not a valid phone number!`
    },
    required: [true, "User phone number required"]
  },
  buyer: {
    type: String,
    enum: ["supplier", "manufacturer"],
    required: [true, "Buyer Type required"]
  },
  categoryIDs: {
    type: Array
  }
});

module.exports = mongoose.model("User", userSchema);
