const express = require("express");
const router = express.Router();
const multer = require("multer");
const checkAuth = require("../middlewares/check-auth");

const ManufacturerController = require("../controllers/manufacturer");

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "./uploads/");
  },
  filename: function(req, file, cb) {
    cb(null, new Date().toISOString() + file.originalname);
  }
});

const fileFilter = (req, file, cb) => {
  // reject a file
  if (
    file.mimetype === "image/jpeg" ||
    file.mimetype === "image/png" ||
    file.mimetype === "application/pdf" ||
    file.mimetype === "application/msword"
  ) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5
  },
  fileFilter: fileFilter
});

router.post(
  "/create-request",
  checkAuth,
  upload.single("attachement"),
  ManufacturerController.manufactuer_supply_request_create
);

router.get(
  "/get-pending-requests",
  checkAuth,
  ManufacturerController.manufacturer_get_supply_pending_requests
);

router.get(
  "/get-accepted-requests",
  checkAuth,
  ManufacturerController.manufacturer_get_supply_accepted_requests
);

router.get(
  "/:requestId",
  checkAuth,
  ManufacturerController.manufactuer_get_request
);

router.patch(
  "/update-request-status/:requestId",
  checkAuth,
  ManufacturerController.manufacturer_update_request_status
);

router.get(
  "/get-addresses/:requestId",
  checkAuth,
  ManufacturerController.manufactuer_get_request_addresses
);

module.exports = router;
