const express = require("express");
const router = express.Router();
const checkAuth = require("../middlewares/check-auth");
const checkToken = require("../middlewares/verifyToken");

const SupplierController = require("../controllers/supplier");

router.get(
  "/get-pending-requests",
  checkAuth,
  checkToken,
  SupplierController.supplier_get_supply_pending_requests
);

router.get(
  "/get-accepted-requests",
  checkAuth,
  checkToken,
  SupplierController.supplier_get_supply_accepted_requests
);

router.patch(
  "/accept-request/:requestId",
  checkAuth,
  SupplierController.supplier_accept_request
);

router.patch(
  "/add-bom/:requestId",
  checkAuth,
  SupplierController.supplier_add_bom
);

module.exports = router;
