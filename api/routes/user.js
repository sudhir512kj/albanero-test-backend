const express = require("express");
const router = express.Router();
const checkAuth = require("../middlewares/check-auth");
const checkToken = require("../middlewares/verifyToken");

const UserController = require("../controllers/user");

router.post("/signup", UserController.user_signup);

router.post("/login", UserController.user_login);

router.get(
  "/account/details",
  checkToken,
  UserController.user_get_account_details
);
router.post("/get-location", UserController.user_get_location);

module.exports = router;
